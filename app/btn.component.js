'use strict';

angular
    .module('buttonApp',[])
    .directive('btn', function () {
        return {
            restrict: "E",
            replace: true,
            transclude: true,
            template: getTemplate,
            scope: {}
        };
        function getTemplate(element, attr) {
            var btnType = (typeof attr.type === 'undefined') ? 'button' : attr.type;
            return '<button class="md-button" type="' + btnType + '" ng-transclude></button>';
        }
    });
    // .controller('btn', function ($scope) {
    //     $scope.title = 'button';
    //     $scope.title4 = 'Warn';
    //     $scope.isDisabled = true;
    // });


// angular
//     .module('buttonApp')
//     .component('btn', {
//         template:
//             '<button>{{title}}</button>',
//         controller: function btnController($scope) {
//             $scope.title = 'button';
//             $scope.title4 = 'Warn';
//             $scope.isDisabled = true;
//         }
//     });
